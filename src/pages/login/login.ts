import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DashboardPage } from "../pages";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  nameUser: boolean;
  general: boolean;
  rutUser: boolean;
  passUser: boolean;

  constructor(public navCtrl: NavController) {
  	
  }

  ionViewDidLoad() {
    this.general = true;
  }

  loginCliente(){
  	this.general = false;
  	this.nameUser = true;
  	this.rutUser = false;
  	this.passUser = false;
  }

  loginUser(){
  	this.general = false;
  	this.nameUser = false;
  	this.rutUser = true;
  	this.passUser = false;
  }

  loginRut(){
  	this.general = false;
  	this.nameUser = false;
  	this.rutUser = false;
  	this.passUser = true;
  }

  login() {
    console.log("Presionamos Login...");
    this.navCtrl.push('DashboardPage');
  }

}
