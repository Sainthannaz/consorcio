import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchesPage } from './searches';

@NgModule({
  declarations: [
    SearchesPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchesPage),
  ],
})
export class SearchesPageModule {}
